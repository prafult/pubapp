package com.team.shard.entites;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class RoundDetails {

	@Id
	@GeneratedValue
	private long id;
	private String name;
	private Timestamp  dateTime;
	private int spinValue;
	@Transient
	private String  displayDate;
	
	
	public RoundDetails() {
		// TODO Auto-generated constructor stub
	}
	
	public RoundDetails(String name, Timestamp sqlDate) {
		this.name=name;
		this.dateTime=sqlDate;
 	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getDateTime() {
		return dateTime;
	}
	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	} 
	 
	 
	
	

	public int getSpinValue() {
		return spinValue;
	}

	public void setSpinValue(int spinValue) {
		this.spinValue = spinValue;
	}
	
	
	@Transient
	public String getDisplayDate() {
		return displayDate;
	}

	@Transient
	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}
 
}

package com.team.shard.service;

import java.text.ParseException;
import java.util.List;

import com.team.shard.entites.RoundDetails;

public interface RoundService {

	void saveRoundDetails(String name, String date) throws ParseException;

	List<RoundDetails> getRoundData() throws ParseException;

}

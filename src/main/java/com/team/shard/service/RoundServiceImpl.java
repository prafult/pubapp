package com.team.shard.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.shard.dao.RoundDAO;
import com.team.shard.entites.RoundDetails;
import com.team.shard.utils.DateTimeUtils;


@Service
@Transactional
public class RoundServiceImpl implements RoundService {
	
	@Autowired
	private RoundDAO roundDAO;
	
	@Override
	
	public void saveRoundDetails(String name,String date) throws ParseException{
		  roundDAO.saveRoundDetails(name,DateTimeUtils.getSqlDate(date));
	}

	@Override
	
	public List<RoundDetails> getRoundData() throws ParseException {

		List<RoundDetails> returnList = new ArrayList<RoundDetails>();
		int counter =0;
		while(counter<=5){
			try{
		List<RoundDetails> list = roundDAO.getRoundData();
		
		LocalDate nextFriday=DateTimeUtils.calcNextFriday(new LocalDate());
		for(RoundDetails data:list){
			RoundDetails newRow = new RoundDetails();
			newRow.setName(data.getName());
			newRow.setDisplayDate(nextFriday.toString());
			nextFriday=DateTimeUtils.calcNextFriday(nextFriday.plusDays(1));
			newRow.setSpinValue(data.getSpinValue());
			returnList.add(newRow);
		}
		return returnList;
			}catch(Exception e){
				e.printStackTrace();
			}
			counter++;
		}
		return returnList;
	}
}

package com.team.shard.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.team.shard.entites.RoundDetails;

@Repository
public class RoundDAOImpl implements RoundDAO  {

	@Autowired
	private SessionFactory sessionFactory;



	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {

		this.sessionFactory = sessionFactory;
	}

	@Override
	public void saveRoundDetails(String name, Timestamp sqlDate) {

		RoundDetails rd = (RoundDetails) sessionFactory.getCurrentSession().createCriteria(RoundDetails.class).add(Restrictions.eq("name", name)).uniqueResult();
		if(rd!=null){
			rd.setDateTime(sqlDate);
		}else{
			rd = new RoundDetails(name,sqlDate);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<RoundDetails> getRoundData() {
		return sessionFactory.getCurrentSession().createCriteria(RoundDetails.class).addOrder(Order.asc("dateTime")).list();

	}
}
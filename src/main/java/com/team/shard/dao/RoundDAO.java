package com.team.shard.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.SessionFactory;

import com.team.shard.entites.RoundDetails;

public interface RoundDAO {
	
	 
 	 
 
	 void setSessionFactory(SessionFactory sessionFactory);

	void saveRoundDetails(String name, Timestamp sqlDate);

	List<RoundDetails> getRoundData();
	 
 

}
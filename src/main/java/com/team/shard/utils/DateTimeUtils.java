package com.team.shard.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.ReadableInstant;

public class DateTimeUtils {
	
	
	public static ReadableInstant getStartOfTodayIST() {
		return new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("IST"))).withTime(0, 0, 0, 00); 
	}

	public static void main(String[] args) throws ParseException {
		
		
		 Long internalVersionNumber = null;
		 
		 System.out.println(internalVersionNumber > 10l);
		
		
/*
        long prev = 0;
        long cur = 1;
        System.out.print(prev+" "+cur);
        for (long i = 1; i <= 100; ++i) {
            System.out.print(" "+(cur + prev)+" " );
            cur = cur + prev;
            prev = cur - prev;
        }*/
    
		
/*		
		System.out.println(getSqlDate("07-03-1979"));
		System.out.println(getSqlDate("2014-10-17","yyy-MM-dd"));
		
		  LocalTime time = new LocalTime(DateTimeZone.forID("GB"));
		  System.out.println(time);
		
		Set<String> ids = DateTimeZone.getAvailableIDs();
		
		for(String id:ids){
			System.out.println(id);
		}
		
*/		
		/*LocalDate nextFriday=DateTimeUtils.calcNextFriday(new LocalDate());
		for(int i=1;i<=8;++i){
			System.out.println(nextFriday);
			nextFriday=DateTimeUtils.calcNextFriday(nextFriday.plusDays(1));;
			
		}*/
	}

	public static  java.sql.Timestamp getSqlDate(String stringTime, String dateFormat) throws ParseException {
		
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date parsed = format.parse(stringTime);
		java.sql.Timestamp sqlDate = new java.sql.Timestamp(parsed.getTime());
		return sqlDate;
		
	}

	public static  java.sql.Timestamp getSqlDate(String stringTime)
			throws ParseException {
		
		
		return getSqlDate(stringTime, "MM-dd-yyy");
	}
	
	 
	public static LocalDate calcNextFriday(LocalDate d) {
		 
		  LocalTime time = new LocalTime(DateTimeZone.forID("GB"));
		  int inc=1;
		  if(d.getDayOfWeek() == DateTimeConstants.FRIDAY ){
			  if(time.getHourOfDay() >=13){
				  inc=1;
			  }else {
				  inc = 0;
			  }
		  }else if(d.getDayOfWeek() < DateTimeConstants.FRIDAY ){
			  inc = 0;
		  }
		  
		  return d.plusWeeks(inc).withDayOfWeek(DateTimeConstants.FRIDAY);
		}
	
	
 
}
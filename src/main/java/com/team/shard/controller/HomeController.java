package com.team.shard.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.team.shard.entites.RoundDetails;
import com.team.shard.service.RoundService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private RoundService roundService;
	private static Map<String,Integer> users = new HashMap<String,Integer>();
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) throws ParseException {
		
		List<RoundDetails> list = roundService.getRoundData();
		
		model.addAttribute("lucky",list.get(0));
		list.remove(0);
 		model.addAttribute("data", list);
		return "home";
	}
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/spin", method = RequestMethod.GET)
	public String getSpinData(Locale locale, Model model) throws ParseException {
		 
		
 		//model.addAttribute("data", getSpinResult(roundService.getRoundData()));
		model.addAttribute("data", getSpinResult(roundService.getRoundData()));
		return "spin";
	}
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/round/{name}/{date}")
	public String addRoundDetails(Model model,@PathVariable String name,@PathVariable String date) throws ParseException {
	// example prafulthakare.com/pub/round/PRAFUL/11-21-2014
		
		
		 roundService.saveRoundDetails(name.toUpperCase(), date);
		
		model.addAttribute("data", name+" "+date);
		
		return "add";
	}
	
	
	private static String getSpinResult(List<RoundDetails> list){
		
		RoundDetails d1 = list.get(0);
		RoundDetails d2 = list.get(1);
		RoundDetails d3 = list.get(2);
		
		 
		return d1.getSpinValue()+":"+d1.getDisplayDate()
				+":"+d2.getSpinValue()+":"+d2.getDisplayDate()
				+":"+d3.getSpinValue()+":"+d3.getDisplayDate();
		
	}
	
	
}

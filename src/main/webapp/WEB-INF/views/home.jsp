<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page session="false"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Winter is Coming</title>

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=2.0">

		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="screen" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.slotmachine.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/spin.js"></script>
		
	</head>

	<body id="bg" class="bodyDark">
	
		<div>
			<div class="content container" style="text-align: right">
				<h1></h1>
				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3 mybroder">
						<div id="machine1" class="slotMachine">
							<div class="slot mike"></div>
							<div class="slot praful"></div>
							<div class="slot dan"></div>
							<div class="slot ruben"></div>
							<div class="slot neil"></div>
							<div class="slot mark"></div>
							<div class="slot peter"></div>
							<div class="slot san"></div>
							<div class="slot akos"></div>
						</div>
						<div id="machine2" class="slotMachine">
							<div class="slot mike"></div>
							<div class="slot praful"></div>
							<div class="slot dan"></div>
							<div class="slot ruben"></div>
							<div class="slot neil"></div>
							<div class="slot mark"></div>
							<div class="slot peter"></div>
							<div class="slot san"></div>
							<div class="slot akos"></div>
						</div>
						<div id="machine3" class="slotMachine">
							<div class="slot mike"></div>
							<div class="slot praful"></div>
							<div class="slot dan"></div>
							<div class="slot ruben"></div>
							<div class="slot neil"></div>
							<div class="slot mark"></div>
							<div class="slot peter"></div>
							<div class="slot san"></div>
							<div class="slot akos"></div>
						</div>
						<div id="machine1Result" class="col-xs-4 machineResult"></div>
						<div id="machine2Result" class="col-xs-4 machineResult"></div>
						<div id="machine3Result" class="col-xs-4 machineResult"></div>
					</div>
 					
				</div>
				<div class="col-md-7 col-md-offset-0">
						<input type="button" class="button-add" id="slotMachineButton1" />
					</div>
			</div>
		</div>
		<div id="output"></div>

<div id="overlay" class="web_dialog_overlay"></div>

<div id="dialog" class="web_dialog">
   <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
         <td class="web_dialog_title">Every Man Must Pay !!</td>
         <td class="web_dialog_title align_right">
            <a href="#" id="btnClose">X</a>
         </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr><td></td></tr>
      <tr>
         <td colspan="1" style="padding-left: 15px;">
            <div id="brands">
            <c:forEach items="${data}" var="round" varStatus="loop">
             	<c:if test="${loop.index >=2}">
                                        <tr>

                                             <td> <c:out value="${round.displayDate}"/></td>
                                             <td> <img height="50px" width="50px" src='https://s3-eu-west-1.amazonaws.com/com.prafulthakare.pub/img/<c:out value="${round.name}"/>.png'></img></td>
                                        </tr>
                 </c:if>                
                </c:forEach>

            </div>
         </td>
      </tr>
      
   </table>
</div>

<audio class="audioTheam" controls preload="none"> 
   <source src="https://s3-eu-west-1.amazonaws.com/com.prafulthakare.pub/audio/got.mp3" type="audio/mpeg">
 </audio>	
  		
	
 	</body>
</html>

			$(document).ready(function(){
			var stopVal1=0;
			var stopVal2=0;
			var stopVal3=0;
			
			var date1='Today';
			var date2=0;
			var date3=0;
			//alert('');
			hideButton();
			
			 $("#btnClose").click(function (e)
                     {
                        HideDialog();
                        showButton();
                        changeDarktBG();
                        e.preventDefault();
                     });
			
			
			$.get( "spin", function( data ) {
				 var responseArray = data.split(":");
				// alert(data);
				stopVal1=responseArray[0]-1;
				stopVal2=responseArray[2]-1;
				stopVal3=responseArray[4]-1;
				
				date1=responseArray[1];
				date2=responseArray[3];
				date3=responseArray[5];
				//alert(stopVal);
				showButton();
				});
			
				var machine1 = $("#machine1").slotMachine({
					active	: 0,
					delay	: 1500,
					randomize : function(activeElementIndex){
							return stopVal1;
							}
				});
				
				var machine2 = $("#machine2").slotMachine({
					active	: 1,
					delay	: 1500,
					randomize : function(activeElementIndex){
							return stopVal2;
							}
				});
				
				var machine3 = $("#machine3").slotMachine({
					active	: 2,
					delay	: 1500,
					randomize : function(activeElementIndex){
							return stopVal3;
							}
				});
				
				function onComplete(active){
					switch(this.element[0].id){
					case 'machine1':
						$("#machine1Result").text(date1);
	 
						break;
					case 'machine2':
						$("#machine2Result").text(date2);
						break;
					case 'machine3':
						$("#machine3Result").text(date3);

						changeBrightBG();
						ShowDialog(false);
						
						break;
				}
					
				}
				
				$("#slotMachineButton1").click(function(){
					
					$(".audioTheam").trigger('play');
					machine1.shuffle(5, onComplete);
					
					setTimeout(function(){
						machine2.shuffle(5, onComplete);
					}, 500);
					
					setTimeout(function(){
						machine3.shuffle(5, onComplete);
					}, 1000);
					
				})
				$(".audioTheam").bind("load",function(){
							//alert("Audio Loaded succesfully");
					});
				
				$(".audioTheam").trigger('load');
 				
			});
			
			
			function ShowDialog(modal)
            {
               $("#overlay").show();
               $("#dialog").fadeIn(600);
               hideButton();
               if (modal)
               {
                  $("#overlay").unbind("click");
               }
               else
               {
                  $("#overlay").click(function (e)
                  {
                     HideDialog();
                     showButton();
                  });
               }
               
              
            }

            function HideDialog()
            {
               $("#overlay").hide();
               $("#dialog").fadeOut(300);
               $(".audioTheam").trigger('pause');
             }
            function hideButton()
            {
            	$("#slotMachineButton1").hide();
            }

            function showButton()
            {
            	$("#slotMachineButton1").show();
            	
            }
            function changeBrightBG(){
            	var body = $('#bg');
				body.fadeOut(2500, function () {
					body.removeClass("bodyDark");
					body.addClass("bodyLight");
					body.fadeIn(1000);
			    });
            }
            
            function changeDarktBG(){
            	var body = $('#bg');
				body.fadeOut(2500, function () {
					body.removeClass("bodyLight");
					body.addClass("bodyDark");
					body.fadeIn(1000);
			    });
            }
            